#ifndef USHAPELET_H
#define USHAPELET_H

#include "DataRow.h"

class UShapelet {

    int tsIndex;
    int location;
    int uShapeLength;
    float gap;
    int randomIndex;

    DataRow<float>* shapelet;

public:

    UShapelet(int tsIndex, int location, int uShapeLength, DataRow<float>* data);

    ~UShapelet();

    bool hasNonZeroDifferences();

    void setGap(float gap);

    float getGap();

    void setRandomIndex(int randomIndex);

    int getRandomIndex();

    DataRow<float>* getShapeletData() const;
};

#endif