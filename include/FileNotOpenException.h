#ifndef FILENOTOPEN_H
#define FILENOTOPEN_H

#include <iostream>
#include <exception>

class FileNotOpenException: public std::exception {
    virtual const char* what() const throw() {
        return "Failed to open file!";
    }
};

#endif