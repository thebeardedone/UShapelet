#ifndef USHAPELETS_H
#define USHAPELETS_H

#include <algorithm>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <utility>
#include <vector>

#include "boost/random/mersenne_twister.hpp"
#include "boost/random/uniform_int_distribution.hpp"

// TODO: remove me
#include <iomanip>

#include "DataContainer.h"
#include "DistanceMatrix.h"
#include "TSMapper.h"
#include "UShapelet.h"

class UShapelets {

    std::size_t numberOfTS;
    std::size_t tsLength;
    std::size_t uShapeLength;
    std::size_t alphabetSize;
    std::size_t saxShapeletLength;

    std::vector<int> classLabels;
    DataContainer<float>* data;
    TSMapper* tsMapper;

    std::vector<UShapelet*> uShapelets;
    std::vector<std::size_t> remainingIndices;

    std::map<std::size_t, std::vector<int>> shapeletHashes;
    std::map<std::size_t, std::vector<int>> shapeletStorage;

    void generateSaxHash(DataContainer<float>* uShapeletsZScore);

    void getRandomProjectionMatrix(DataContainer<int>* saxTsShapelets, DataContainer<float>* uShapeletsZScore, std::size_t randomProjections);

    DataContainer<int>* sort(std::size_t uShapeletSize, std::size_t uShapeletCount);

    void computeGap(UShapelet* shapelet);

    UShapelet* setGap(std::size_t index, DataContainer<int>* shapelets, int classNumber);

    void findFastUShapelet();

public:

    UShapelets(std::vector<int> classLabels, DataContainer<float>* data, DataRow<float>* saxTableRow, std::size_t alphabetSize, std::size_t uShapeLength);

    ~UShapelets();

    std::size_t getRemainingIndicesLength();

    void findBestUShapelet();
};

#endif