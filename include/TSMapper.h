#ifndef TSMAPPER_H
#define TSMAPPER_H

#include "DataContainer.h"
#include "DataRow.h"

class TSMapper {

    std::size_t uShapeLength;

    DataRow<float>* cutPoints;

    DataContainer<int>* symbolicData;
    std::vector<std::size_t> pointers;

    void mapToString(DataRow<float>* paa, std::size_t alphabetSize, DataRow<int>* result);

public:

    TSMapper(std::size_t uShapeLength, DataRow<float>* cutPoints);

    ~TSMapper();

    std::size_t getNumberOfRows();

    DataContainer<int>* getData();

    std::size_t getPointer(std::size_t index);

    void printPointers();

    void timeseries2symbol(DataContainer<float>* uShapeletsZScore, std::size_t alphabetSize, std::size_t saxShapeletLength);
};

#endif