#ifndef SAXTABLEPARSER_H
#define SAXTABLEPARSER_H

#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <limits>

#include "DataRow.h"
#include "InvalidAlphabetSizeException.h"

class SaxTableParser {

public:

    SaxTableParser();

    static DataRow<float>* getSaxTable(std::string filename, std::size_t alphabetSize);

};

#endif