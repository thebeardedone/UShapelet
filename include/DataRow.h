#ifndef DATAROW_H
#define DATAROW_H

#include <cstdlib>
#include <iostream>
#include <math.h>

template <class T>
class DataRow {

    bool foreignReferences = false;
    std::size_t columns;
    T* data;

public:

    enum class StdNormalization { DEFAULT, OBSERVATIONS };

    DataRow() {
        this->columns = 0;
        data = nullptr;
    }

    DataRow(std::size_t columns) {
        this->columns = columns;
        data = new T[columns];
        initData(0);
    }

    DataRow(std::size_t columns, bool foreignReferences) {
        this->columns = columns;
        this->foreignReferences = foreignReferences;

        if(!foreignReferences) {
            data = new T[columns];

            for(std::size_t i = 0; i < columns; i++)
                data[i] = 0;
        } else
            data = nullptr;
    }

    DataRow(const DataRow<T>& dataRow) {
        this->columns = dataRow.getNumberOfColumns();
        data = new T[columns];
        setData(dataRow.getRawPtr());
    }

    ~DataRow() {
        if(!foreignReferences)
            delete[] data;
    }

    std::size_t getNumberOfColumns() const {
        return columns;
    }

    T* getRawPtr() const {
        return data;
    }

    T getData(std::size_t column) {
        return data[column];
    }

    void setData(std::size_t column, T value) {
        data[column] = value;
    }

    void setData(T* values) {
        if(foreignReferences)
            data = values;
        else {
            for(std::size_t i = 0; i < columns; i++)
                data[i] = values[i];
        }
    }

    void setData(std::size_t writeOffset, std::size_t numberOfValues, const T* values) {
        for(std::size_t i = 0; i < numberOfValues; i++)
            data[writeOffset + i] = values[i];
    }

    void initData(T value) {
        for(std::size_t i = 0; i < columns; i++)
            data[i] = value;
    }

    void initData(std::size_t offset, T value) {
        for(std::size_t i = offset; i < columns; i++)
            data[i] = value;
    }

    void printData() {
        for(std::size_t i = 0; i < columns; i++)
            std::cout << data[i] << " ";
        std::cout << std::endl;
    }

    void printDataWithLineBreaks() {
        for(std::size_t i = 0; i < columns; i++)
            std::cout << data[i] << std::endl;
    }

    void printData(std::size_t start, std::size_t end) {
        for(std::size_t i = start; i < end; i++)
            std::cout << data[i] << " ";
        std::cout << std::endl;
    }

    std::size_t getInt() {
        std::size_t value = 0;

        for(std::size_t i = 0; i < columns; i++) {
            value += data[i] * pow(10, columns - i - 1);
        }

        return value;
    }

    void copyValues(std::size_t startColumn, std::size_t endColumn, T* dest, std::size_t destOffset) {
        std::size_t destCounter = destOffset;
        for(std::size_t i = startColumn; i < endColumn; i++) {
            dest[destCounter] = data[i];
            destCounter++;
        }
    }

    bool compare(DataRow<T>* values) {
        if(columns != values->getNumberOfColumns())
            return false;
        
        for(std::size_t i = 0; i < columns; i++) {
            if(data[i] != values->getData(i))
                return false;
        }

        return true;
    }

    void addValue(T value) {
        addValue(0, columns, value);
    }

    void addValue(std::size_t column, T value) {
        data[column] += value;
    }

    void addValue(std::size_t startColumn, std::size_t endColumn, T value) {
        if(endColumn == 0)
            endColumn = columns;

        for(std::size_t i = startColumn; i < endColumn; i++)
            data[i] += value;
    }

    void scale(float value) {
        return scale(0, columns, value);
    }

    void scale(std::size_t startColumn, std::size_t endColumn, float value) {
        if(endColumn == 0)
            endColumn = columns;

        for(std::size_t i = startColumn; i < endColumn; i++)
            data[i] *= value;
    }

    float mean() {
        return mean(0, columns);
    }

    float mean(std::size_t startColumn, std::size_t endColumn) {

        float sum = 0;

        if(endColumn == 0)
            endColumn = columns;

        for(std::size_t i = startColumn; i < endColumn; i++)
            sum += data[i];
        
        return sum / (endColumn - startColumn);
    }

    float variance(std::size_t startColumn, std::size_t endColumn, StdNormalization normalization) {

        if(endColumn == 0)
            endColumn = columns;

        float mean = this->mean(startColumn, endColumn);

        float variance = 0;

        for(std::size_t i = startColumn; i < endColumn; i++)
            variance += pow(data[i] - mean, 2);

        if(normalization == StdNormalization::DEFAULT)
            return variance / (endColumn - startColumn - 1);
        else
            return variance / (endColumn - startColumn);
    }

    float std(StdNormalization normalization) {
        return sqrt(variance(0, columns, normalization));
    }

    float std(std::size_t startColumn, std::size_t endColumn) {
        return sqrt(variance(startColumn, endColumn, StdNormalization::DEFAULT));
    }

    float std(std::size_t startColumn, std::size_t endColumn, StdNormalization normalization) {
        return sqrt(variance(startColumn, endColumn, normalization));
    }

    T getMedian() {
        if(columns % 2 != 0)
            return data[columns/2 + 1];
        else
            return (data[columns/2] + data[columns/2 + 1]) / 2.0;
    }

    void sort(DataRow<std::size_t>* indices, std::size_t left, std::size_t right) {
        std::size_t i = left;
        std::size_t j = right;
        T pivot = data[(left + right) / 2];

        // partition
        while (i <= j) {
            while (data[i] < pivot)
                i++;

            while (data[j] > pivot)
                j--;

            if (i <= j) {
                T tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
                
                if(indices != nullptr) {
                    std::size_t tmpIndex = indices->getData(i);
                    indices->setData(i, indices->getData(j));
                    indices->setData(j, tmpIndex);
                }
                
                i++;
                j--;
            }
        }

        // recursion
        if (left < j)
            sort(indices, left, j);
        if (i < right)
            sort(indices, i, right);
    }
};

#endif