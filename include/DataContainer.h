#ifndef DATACONTAINER_H
#define DATACONTAINER_H

#include <cstdlib>
#include <vector>

#include "DataRow.h"

template <class T>
class DataContainer {

    std::size_t rows;
    std::size_t columns;
    DataRow<T>** data;

public:

    DataContainer(std::size_t rows, std::size_t columns) {
        this->rows = rows;
        this->columns = columns;
        data = new DataRow<T>*[rows];

        for(std::size_t i = 0; i < rows; i++)
            data[i] = new DataRow<T>(columns);
    }

    DataContainer(std::size_t rows, std::size_t columns, bool foreignReferences) {
        this->rows = rows;
        this->columns = columns;
        data = new DataRow<T>*[rows];

        for(std::size_t i = 0; i < rows; i++)
            data[i] = new DataRow<T>(columns, foreignReferences);
    }

    DataContainer(const DataContainer<T>& dataContainer) {
        rows = dataContainer.getNumberOfRows();
        columns = dataContainer.getNumberOfColumns();
        data = new DataRow<T>*[rows];

        for(std::size_t i = 0; i < rows; i++)
            data[i] = new DataRow<T>(*dataContainer.getRow(i));
    }

    ~DataContainer() {
        for(std::size_t i = 0; i < rows; i++)
            delete data[i];

        delete[] data;
    }

    std::size_t getNumberOfRows() const {
        return rows;
    }

    std::size_t getNumberOfColumns() const {
        return columns;
    }

    DataRow<T>* getRow(std::size_t row) const {
        return data[row];
    }

    DataContainer<T>* getRows(std::size_t start, std::size_t end) {

        DataContainer<T>* matchingRows = new DataContainer<T>(end - start, columns, true);

        std::size_t index = 0;
        for(std::size_t i = start; i < end; i++) {
            matchingRows->getRow(index)->setData(getRow(i)->getRawPtr());
            index++;
        }
        return matchingRows;
    }

    DataContainer<T>* getRows(std::vector<std::size_t> indices) {

        DataContainer<T>* matchingRows = new DataContainer<T>(indices.size(), columns, true);

        for(std::size_t i = 0; i < indices.size(); i++)
            matchingRows->getRow(i)->setData(getRow(indices.at(i))->getRawPtr());

        return matchingRows;
    }

    void setData(std::size_t row, std::size_t column, T value) {
        data[row]->setData(column, value);
    }

    void initData(T value) {
        for(std::size_t i = 0; i < rows; i++)
            data[i]->initData(value);
    }

    void printData() {
        for(std::size_t i = 0; i < rows; i++)
            getRow(i)->printData();
    }

    std::vector<std::size_t> findMatchingRows(std::size_t column, std::size_t criteria) {
        std::vector<std::size_t> indices = std::vector<std::size_t>();

        for(std::size_t i = 0; i < rows; i++) {
            if((std::size_t) data[i]->getData(column) == criteria)
                indices.push_back(i);
        }

        return indices;
    }

    void setOrder(DataRow<std::size_t>* order) {
        for(std::size_t i = 0; i < order->getNumberOfColumns(); i++) {
            DataRow<T>* tmp = data[i];
            data[i] = data[order->getData(i)];
            data[order->getData(i)] = tmp;
        }
    }
};

#endif