#ifndef DISTANCEMATRIX_H
#define DISTANCEMATRIX_H

#include<fftw3.h>

#include "DataContainer.h"
#include "DataRow.h"
#include "UShapelet.h"

class DistanceMatrix {

    DataContainer<float>* distance;
    DataContainer<float>* locations;
    DataRow<float>* x;
    DataRow<float>* y;
    fftw_complex* input;

    fftw_complex* fft(long fftType, std::size_t size);

    void setInput(DataRow<float>* data);

    void findNearestNeighbour(std::size_t dataRow, std::size_t shapeletRow);

public:

    DistanceMatrix(std::size_t rows, std::size_t columns, std::size_t sizeY);

    ~DistanceMatrix();

    void calculateDistanceMatrix(UShapelet* shapelet, DataContainer<float>* data);
};

#endif