#ifndef INVALIDALPHABETSIZEEXCEPTION_H
#define INVALIDALPHABETSIZEEXCEPTION_H

#include <iostream>
#include <exception>

class InvalidAlphabetSizeException: public std::exception {
    virtual const char* what() const throw() {
        return "Invalid alphabet size! Check the SAX table to ensure that the provided value is present.";
    }
};

#endif