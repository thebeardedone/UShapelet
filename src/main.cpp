#include <fstream>
#include <iostream>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>

#include "FileNotOpenException.h"
#include "SaxTableParser.h"
#include "UShapelets.h"

void printArgs() {
    std::cout << "uShapelet Parameters: " << std::endl;
    std::cout << "  --data=<path_to_data>" << std::endl;
    std::cout << "  --saxTable=<path_to_sax_table>" << std::endl;
    std::cout << "  --alphabetSize=<number>" << std::endl;
    std::cout << "  --uShapeLength=<number>" << std::endl;
    std::cout << "ex. ./uShapelet --data=data.txt --saxTable=saxTable.json --alphabetSize=4 --uShapeLength=30" << std::endl;
    std::cout << "ex. ./uShapelet --data=../data/FourClasses.txt --saxTable=../data/saxTable.json --alphabetSize=4 --uShapeLength=30" << std::endl;
}

template<class T>
DataContainer<T>* readData(std::string dataLocation) {
    std::ifstream inputStream;
    inputStream.open(dataLocation);

    if(!inputStream.is_open())
        throw new FileNotOpenException;

    std::size_t rows = 0;
    std::size_t columns = 0;

    std::string line = "";

    while(getline(inputStream, line)) {
        if(columns == 0) {
            std::vector<std::string> values;
            boost::split(values, line, boost::is_any_of(","));
            columns = values.size();
        }
        rows++;
    }

    DataContainer<T>* data = new DataContainer<T>(rows, columns);

    inputStream.clear();
    inputStream.seekg(0, std::ios::beg);

    std::size_t row = 0;
    while(getline(inputStream, line)) {
        std::vector<std::string> values;
        boost::split(values, line, boost::is_any_of(","));
        for(std::size_t column = 0; column < values.size(); column++) {
            data->setData(row, column, stof(values.at(column)));
        }
        row++;
    }

    inputStream.close();

    return data;
}

int main(int argc, char** argv) {

    std::string dataLocation;
    std::string saxTableLocation;
    std::size_t alphabetSize;
    std::size_t uShapeLength;

    if(argc < 5) {
        printArgs();
        return 1;
    }

    try {
        for(int i = 1; i < argc; i++) {
            if (boost::starts_with(argv[i], "--data="))
                dataLocation = argv[i]+7;
            else if(boost::starts_with(argv[i], "--saxTable="))
                saxTableLocation = argv[i]+11;
            else if(boost::starts_with(argv[i], "--alphabetSize="))
                alphabetSize = (std::size_t)boost::lexical_cast<int>(argv[i]+15);
            else if(boost::starts_with(argv[i], "--uShapeLength="))
                uShapeLength = (std::size_t)boost::lexical_cast<int>(argv[i]+15);
            else {
                std::cout << "Invalid parameter found: " << argv[i] << std::endl;
                printArgs();
                return 1;
            }
        }
    } catch (boost::bad_lexical_cast exception) {
        std::cout << "Invalid value passed: " << std::endl;
        exception.what();
        printArgs();
        return 1;
    }

    std::unique_ptr<SaxTableParser> saxTableParser = std::make_unique<SaxTableParser>();

    // Algo initialization

    // std::size_t minGap = 0;
    std::vector<int> classLabels = std::vector<int>();

    std::unique_ptr<UShapelets> uShapelets = std::make_unique<UShapelets>(classLabels, readData<float>(dataLocation), saxTableParser->getSaxTable(saxTableLocation, alphabetSize), alphabetSize, uShapeLength);
    
    std::vector<std::size_t> label = std::vector<std::size_t>();

    // std::size_t currentCluster = 1;

    while(uShapelets->getRemainingIndicesLength() > 3) {

        uShapelets->findBestUShapelet();

        // TODO: remove break
        break;

    }

    return 0;
}