#include "UShapelet.h"

UShapelet::UShapelet(int tsIndex, int location, int uShapeLength, DataRow<float>* data) {
    this->tsIndex = tsIndex;
    this->location = location;
    this->uShapeLength = uShapeLength;

    this->shapelet = new DataRow<float>(uShapeLength);
    
    for(int i = 0; i < uShapeLength; i++)
        this->shapelet->setData(i, data->getData(location + i));
}

UShapelet::~UShapelet() {
    delete shapelet;
}

bool UShapelet::hasNonZeroDifferences() {

    for(int i = 1; i < uShapeLength; i++) {
        if(shapelet->getData(i) - shapelet->getData(i - 1))
            return true;
    }

    return false;
}

void UShapelet::setGap(float gap) {
    this->gap = gap;
}

float UShapelet::getGap() {
    return gap;
}

void UShapelet::setRandomIndex(int randomIndex) {
    this->randomIndex = randomIndex;
}

int UShapelet::getRandomIndex() {
    return randomIndex;
}

DataRow<float>* UShapelet::getShapeletData() const {
    return shapelet;
}