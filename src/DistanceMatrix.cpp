#include "DistanceMatrix.h"

DistanceMatrix::DistanceMatrix(std::size_t rows, std::size_t columns, std::size_t uShapeLength) {
    distance = new DataContainer<float>(rows, columns);
    locations = new DataContainer<float>(rows, columns);

    std::size_t dataRowSize = 2 * (columns + 1);

    x = new DataRow<float>(dataRowSize);
    y = new DataRow<float>(dataRowSize);
    input = new double[dataRowSize][2];
}

DistanceMatrix::~DistanceMatrix() {
    delete distance;
    delete locations;
    delete x;
    delete y;
    delete input;
}

fftw_complex* DistanceMatrix::fft(long fftType, std::size_t size) {

    fftw_complex* output = new double[size][2];
    
    fftw_plan plan = fftw_plan_dft_1d(size, input, output, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    return output;
}

void DistanceMatrix::setInput(DataRow<float>* data) {
    for(std::size_t i = 0; i < data->getNumberOfColumns(); i++) {
        input[i][0] = data->getData(i);
        input[i][1] = 0;
    }
}

fftw_complex* multiply(fftw_complex* X, fftw_complex* Y, std::size_t size) {
    fftw_complex* z = new double[size][2];

    for(std::size_t i = 0; i < size; i++) {
        z[i][0] = X[i][0]*Y[i][0] - X[i][1]*X[i][1];
        z[i][1] = X[i][1]*Y[i][0] + X[i][0]*X[i][1];
    }

    return z;
}

void DistanceMatrix::findNearestNeighbour(std::size_t dataRow, std::size_t shapeletRow) {
    y->addValue(-y->mean());
    y->scale(y->std(DataRow<float>::StdNormalization::OBSERVATIONS));
    
    setInput(x);
    fftw_complex* xTransformed = fft(FFTW_FORWARD, x->getNumberOfColumns());

    setInput(y);
    fftw_complex* yTransformed = fft(FFTW_FORWARD, y->getNumberOfColumns());

    fftw_complex* z = multiply(xTransformed, yTransformed, x->getNumberOfColumns());

    for(std::size_t i = 0; i < x->getNumberOfColumns(); i++) {
        input[i][0] = z[i][0];
        input[i][1] = z[i][1];
    }
    fftw_complex* zIfftw = fft(FFTW_BACKWARD, x->getNumberOfColumns());

    delete xTransformed;
    delete yTransformed;
    delete z;
    delete zIfftw;
}

void DistanceMatrix::calculateDistanceMatrix(UShapelet* shapelet, DataContainer<float>* data) {
    for(std::size_t i = 0; i < data->getNumberOfRows(); i++) {
        for(std::size_t j = 0; j < data->getNumberOfColumns(); j++) {
            x->initData(0);
            x->setData(1, data->getNumberOfColumns(), data->getRow(i)->getRawPtr());
            
            y->initData(0);
            
            // reverse order
            for(std::size_t k = 0; k < shapelet->getShapeletData()->getNumberOfColumns(); k++)
                y->setData(shapelet->getShapeletData()->getNumberOfColumns() - 1 - k, shapelet->getShapeletData()->getData(k));

            findNearestNeighbour(j, i);
        }
    }
}