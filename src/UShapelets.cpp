#include "UShapelets.h"

UShapelets::UShapelets(std::vector<int> classLabels, DataContainer<float>* data, DataRow<float>* saxTableRow, std::size_t alphabetSize, std::size_t uShapeLength) {

    this->classLabels = classLabels;
    this->numberOfTS = data->getNumberOfRows();
    this->tsLength = data->getNumberOfColumns();
    this->uShapeLength = uShapeLength;
    this->alphabetSize = alphabetSize;
    this->saxShapeletLength = 16;
    
    this->data = data;

    tsMapper = new TSMapper(uShapeLength, saxTableRow);
    uShapelets = std::vector<UShapelet*>();

    shapeletHashes = std::map<std::size_t, std::vector<int>>();
    shapeletStorage = std::map<std::size_t, std::vector<int>>();

    remainingIndices = std::vector<std::size_t>();
    for(std::size_t i = 0; i < numberOfTS; i++)
        remainingIndices.push_back(i);
}

UShapelets::~UShapelets() {

    delete tsMapper;

    for(std::size_t i = 0; i < uShapelets.size(); i++)
        delete uShapelets.at(i);
    
    delete data;
}

std::size_t UShapelets::getRemainingIndicesLength() {
    return remainingIndices.size();
}

void UShapelets::generateSaxHash(DataContainer<float>* uShapeletsZScore) {

    shapeletHashes.clear();
    shapeletStorage.clear();

    for(std::size_t i = 0; i < numberOfTS; i++) {
        std::vector<std::size_t> indices = uShapeletsZScore->findMatchingRows(0, i);

        DataContainer<float>* matchingUShapeletsZScore = uShapeletsZScore->getRows(indices);
        std::size_t lastPrevIndex = indices.at(0);

        tsMapper->timeseries2symbol(matchingUShapeletsZScore, alphabetSize, saxShapeletLength);

        for(std::size_t j = 0; j < tsMapper->getNumberOfRows(); j++) {
            std::size_t saxKey = tsMapper->getData()->getRow(j)->getInt();
            
            std::map<std::size_t, std::vector<int>>::iterator shapeletsHashesIterator = shapeletHashes.find(saxKey);

            if(shapeletsHashesIterator != shapeletHashes.end()) {
                if(std::find(shapeletsHashesIterator->second.begin(), shapeletsHashesIterator->second.end(), i) == shapeletsHashesIterator->second.end())
                    shapeletsHashesIterator->second.push_back(i);
            } else {
                std::pair<std::size_t, std::vector<int>> shapeletHashPair = std::pair<std::size_t, std::vector<int>>();
                shapeletHashPair.first = saxKey;
                shapeletHashPair.second.push_back(i);
                shapeletHashes.insert(shapeletHashPair);

                std::pair<std::size_t, std::vector<int>> shapeletsStoragePair = std::pair<std::size_t, std::vector<int>>();
                shapeletsStoragePair.first = saxKey;
                shapeletStorage.insert(shapeletsStoragePair);
            }

            std::map<std::size_t, std::vector<int>>::iterator shapeletsStorageIterator = shapeletStorage.find(saxKey);

            if(j == tsMapper->getNumberOfRows() - 1) {
                for(std::size_t k = lastPrevIndex + tsMapper->getPointer(j); k < lastPrevIndex + matchingUShapeletsZScore->getNumberOfRows(); k++)
                    shapeletsStorageIterator->second.push_back(k);
            } else {
                for(std::size_t k = lastPrevIndex + tsMapper->getPointer(j); k < lastPrevIndex + tsMapper->getPointer(j + 1); k++)
                    shapeletsStorageIterator->second.push_back(k);
            }
        }

        delete matchingUShapeletsZScore;
    }
}

void UShapelets::getRandomProjectionMatrix(DataContainer<int>* saxTsShapelets, DataContainer<float>* uShapeletsZScore, std::size_t randomProjections) {
    
    generateSaxHash(uShapeletsZScore);
    
    std::size_t maskedNumbers = 3;

    for(std::map<std::size_t, std::vector<int>>::iterator iterator = shapeletStorage.begin(); iterator != shapeletStorage.end(); iterator++) {
        for(std::size_t i = 0; i < iterator->second.size(); i++)
            saxTsShapelets->getRow(iterator->second.at(i))->initData(3, shapeletHashes.at(iterator->first).size());
    }

    boost::random::mt19937 numberGenerator;
    boost::random::uniform_int_distribution<> distribution(1, saxShapeletLength);

    std::unique_ptr<DataContainer<int>> masks = std::make_unique<DataContainer<int>>(randomProjections, maskedNumbers);

    for(std::size_t i = 0; i < randomProjections; i++) {
        std::set<int> maskedPlace = std::set<int>();
        
        while(maskedPlace.size() < maskedNumbers)
            maskedPlace.insert(distribution(numberGenerator));

        // TODO: remove this block
        maskedPlace.clear();
        switch(i) {
            case 0:
                maskedPlace.insert(3);
                maskedPlace.insert(14);
                maskedPlace.insert(15);
                break;
            case 1:
                maskedPlace.insert(2);
                maskedPlace.insert(11);
                maskedPlace.insert(15);
                break;
            case 2:
                maskedPlace.insert(5);
                maskedPlace.insert(9);
                maskedPlace.insert(16);
                break;
            case 3:
                maskedPlace.insert(8);
                maskedPlace.insert(13);
                maskedPlace.insert(16);
                break;
            case 4:
                maskedPlace.insert(3);
                maskedPlace.insert(7);
                maskedPlace.insert(15);
                break;
            case 5:
                maskedPlace.insert(11);
                maskedPlace.insert(13);
                maskedPlace.insert(16);
                break;
            case 6:
                maskedPlace.insert(1);
                maskedPlace.insert(14);
                maskedPlace.insert(15);
                break;
            case 7:
                maskedPlace.insert(11);
                maskedPlace.insert(12);
                maskedPlace.insert(13);
                break;
            case 8:
                maskedPlace.insert(3);
                maskedPlace.insert(7);
                maskedPlace.insert(11);
                break;
            case 9:
                maskedPlace.insert(1);
                maskedPlace.insert(5);
                maskedPlace.insert(12);
                break;
        }
        // TODO: end remove

        std::size_t columnCounter = 0;
        for(std::set<int>::iterator iterator = maskedPlace.begin(); iterator != maskedPlace.end(); iterator++) {
            masks->getRow(i)->setData(columnCounter, *iterator);
            columnCounter++;
        }

        DataRow<std::size_t>* keys = new DataRow<std::size_t>(shapeletStorage.size());
        columnCounter = 0;
        for(std::map<std::size_t, std::vector<int>>::iterator iterator = shapeletStorage.begin(); iterator != shapeletStorage.end(); iterator++) {
            keys->setData(columnCounter, iterator->first);
            columnCounter++;
        }

        for(columnCounter = 0; columnCounter < maskedNumbers; columnCounter++) {
            int wildCard = masks->getRow(i)->getData(columnCounter);
            for(std::size_t j = 0; j < keys->getNumberOfColumns(); j++)
                keys->setData(j, ((std::size_t) (keys->getData(j) / pow(10, wildCard))) * pow(10, wildCard) + (keys->getData(j) % (std::size_t) pow(10, wildCard - 1)));
        }

        std::map<std::size_t, std::size_t> counts = std::map<std::size_t, std::size_t>();

        for(std::size_t k = 0; k < keys->getNumberOfColumns(); k++) {
            std::map<std::size_t, std::size_t>::iterator countsIterator = counts.find(keys->getData(k));
            if(countsIterator == counts.end())
                counts.insert(std::pair<std::size_t, std::size_t>(keys->getData(k), 1));
            else
                countsIterator->second = countsIterator->second + 1;
        }

        for(std::map<std::size_t, std::size_t>::iterator iterator = counts.begin(); iterator != counts.end(); iterator++) {
            std::set<std::size_t> hashValues = std::set<std::size_t>();
            std::vector<std::size_t> storagePositions = std::vector<std::size_t>();

            if(iterator->second > 1) {
                
                std::size_t localCounter = 0;
                for(columnCounter = 0; columnCounter < keys->getNumberOfColumns(); columnCounter++) {
                    if(keys->getData(columnCounter) == iterator->first) {
                        
                        auto hashIterator = std::next(shapeletHashes.begin(), localCounter);
                        if(hashIterator != shapeletHashes.end()) {
                            for(auto value : hashIterator->second)
                                hashValues.insert(value);
                        }

                        auto storageIterator = std::next(shapeletStorage.begin(), localCounter);
                        if(storageIterator != shapeletStorage.end()) {
                            for(auto value : storageIterator->second)
                                storagePositions.push_back(value);
                        }
                    }
                    localCounter++;
                }

                for(auto position : storagePositions)
                    saxTsShapelets->setData(position, i + 3, hashValues.size());

            }
        }

        delete keys;
    }
}

DataContainer<int>* UShapelets::sort(std::size_t uShapeletSize, std::size_t uShapeletCount) {
    float lowerBound = std::max(numberOfTS * 0.1, 2.0);
    float upperBound = numberOfTS * 0.9;
    
    std::size_t randomProjections = 10;

    DataContainer<float>* uShapeletsZScore = new DataContainer<float>(uShapeletCount, uShapeLength + 2);

    std::size_t currentRow = 0;
    for(std::size_t i = 0; i < numberOfTS; i++) {

        DataRow<float>* dataRow = data->getRow(i);

        for(std::size_t j = 0; j < uShapeletSize; j++) {
            uShapeletsZScore->setData(currentRow, 0, i);
            uShapeletsZScore->setData(currentRow, 1, j);

            std::size_t lastElement = j + uShapeLength;

            DataRow<float>* currentUShapletZScore = uShapeletsZScore->getRow(currentRow);

            if(dataRow->std(j, lastElement) > 0.0000001) {
                dataRow->copyValues(j, lastElement, currentUShapletZScore->getRawPtr(), 2);
                float std = currentUShapletZScore->std(2, 0);
                currentUShapletZScore->addValue(2, 0, -currentUShapletZScore->mean(2, 0));
                currentUShapletZScore->scale(2, 0, 1/std);
            }
            
            currentRow++;
        }
    }

    DataContainer<int>* saxTsShapelets = new DataContainer<int>(uShapeletCount, randomProjections + 3);
    saxTsShapelets->initData(1);

    currentRow = 0;
    for(std::size_t i = 1; i <= numberOfTS; i++) {
        for(std::size_t j = 1; j <= uShapeletSize; j++) {
            saxTsShapelets->setData(currentRow, 0, i);
            saxTsShapelets->setData(currentRow, 1, j);
            saxTsShapelets->setData(currentRow, 2, 0);
            currentRow++;
        }
    }

    getRandomProjectionMatrix(saxTsShapelets, uShapeletsZScore, randomProjections);
    DataContainer<int>* saxTsShapeletsCopy = new DataContainer<int>(*saxTsShapelets);

    DataRow<bool>* indicesToDelete = new DataRow<bool>(uShapeletCount);
    std::vector<std::size_t> indicesToKeep = std::vector<std::size_t>();

    std::size_t numberOfRowsToDelete = 0;
    for(std::size_t i = 0; i < uShapeletCount; i++) {
        std::size_t count = 0;
        for(std::size_t j = 3; j < saxTsShapelets->getNumberOfColumns(); j++) {
            if(saxTsShapelets->getRow(i)->getData(j) > upperBound || saxTsShapelets->getRow(i)->getData(j) < lowerBound)
                count++;
        }

        if(count > randomProjections * 0.5) {
            indicesToDelete->setData(i, true);
            numberOfRowsToDelete++;
        } else {
            indicesToDelete->setData(i, false);
            indicesToKeep.push_back(i);
        }
    }

    DataContainer<int>* sanitizedSaxTsShapelets = new DataContainer<int>(uShapeletCount - numberOfRowsToDelete, randomProjections + 3);

    std::size_t row = 0;
    for(std::size_t i = 0; i < indicesToDelete->getNumberOfColumns(); i++) {
        if(!indicesToDelete->getData(i)) {
            for(std::size_t j = 0; j < saxTsShapelets->getNumberOfColumns(); j++) {
                sanitizedSaxTsShapelets->setData(row, j, saxTsShapelets->getRow(i)->getData(j));
            }
            row++;
        }
    }

    DataRow<std::size_t>* indices = new DataRow<std::size_t>(sanitizedSaxTsShapelets->getNumberOfRows());
    DataRow<float>* standardDeviations = new DataRow<float>(sanitizedSaxTsShapelets->getNumberOfRows());

    for(std::size_t i = 0; i < standardDeviations->getNumberOfColumns(); i++) {
        indices->setData(i, i);
        standardDeviations->setData(i, sanitizedSaxTsShapelets->getRow(i)->std(3, sanitizedSaxTsShapelets->getNumberOfColumns()));
    }

    standardDeviations->sort(indices, 0, standardDeviations->getNumberOfColumns() - 1);

    std::size_t unsortedIndex = 0;
    for(std::size_t i = 1; i < indices->getNumberOfColumns(); i++) {
        if(i - unsortedIndex > 0 && standardDeviations->getData(i) != standardDeviations->getData(unsortedIndex)) {
            indices->sort(nullptr, unsortedIndex, i-1);
            unsortedIndex = i;
        }
    }

    if(standardDeviations->getNumberOfColumns() > 0) {
        float median = standardDeviations->getMedian();
        
        std::size_t sortOffset = 0;
        for(std::size_t i = 0; i < standardDeviations->getNumberOfColumns(); i++) {
            if(standardDeviations->getData(i) <= median)
                sortOffset++;
            else
                break;
        }

        std::random_shuffle(indices->getRawPtr(), indices->getRawPtr() + sortOffset);
        sanitizedSaxTsShapelets->setOrder(indices);
    }

    std::random_shuffle(indicesToKeep.begin(), indicesToKeep.end());

    // Re-order original saxTsShapelets object
    for(std::size_t i = 0; i < sanitizedSaxTsShapelets->getNumberOfRows(); i++) {
        for(std::size_t j = 0; j < sanitizedSaxTsShapelets->getNumberOfColumns(); j++)
            saxTsShapelets->setData(i, j, sanitizedSaxTsShapelets->getRow(i)->getData(j));
    }

    for(std::size_t i = 0; i < indicesToKeep.size(); i++) {
        for(std::size_t j = 0; j < saxTsShapelets->getNumberOfColumns(); j++)
            saxTsShapelets->setData(i + sanitizedSaxTsShapelets->getNumberOfRows(), j, saxTsShapeletsCopy->getRow(indicesToKeep.at(i))->getData(j));
    }

    delete uShapeletsZScore;
    delete saxTsShapeletsCopy;
    delete indicesToDelete;
    delete sanitizedSaxTsShapelets;
    delete indices;
    delete standardDeviations;

    return saxTsShapelets;
}

void UShapelets::computeGap(UShapelet* shapelet) {
    DistanceMatrix* distanceMatrix = new DistanceMatrix(data->getNumberOfRows(), data->getNumberOfColumns(), uShapeLength);

    distanceMatrix->calculateDistanceMatrix(shapelet, data);

    delete distanceMatrix;
}

UShapelet* UShapelets::setGap(std::size_t index, DataContainer<int>* shapelets, int classNumber) {
    std::vector<int> classLabelsCopy = std::vector<int>();
    std::copy(classLabels.begin(), classLabels.end(), classLabelsCopy.begin());
    int ts = shapelets->getRow(index)->getData(0);

    UShapelet* shapelet = new UShapelet(ts, shapelets->getRow(index)->getData(1), uShapeLength, data->getRow(ts));

    if(!shapelet->hasNonZeroDifferences()) {
        shapelet->setGap(0);
        shapelet->setRandomIndex(0);
    } else {
        if(classNumber > 2) {
            int currentClass = classLabels.at(ts);
            for(std::size_t i = 0; i < classLabelsCopy.size(); i++) {
                if(classLabelsCopy.at(i) != currentClass)
                    classLabelsCopy.at(i) = currentClass - 1;
            }
        }
        computeGap(shapelet);
    }

    return shapelet;
}

void UShapelets::findFastUShapelet() {

    std::size_t uShapeletSize = tsLength - uShapeLength + 1;
    std::size_t uShapeletCount = numberOfTS * uShapeletSize;

    DataContainer<int>* bestShapelets = sort(uShapeletSize, uShapeletCount);

    std::size_t currentMaxGap = 0;
    std::size_t bestShIndex = 0;
    std::size_t computationStop = 1;
    int classNumber = classLabels.size();
    DataRow<int>* randomIndices = new DataRow<int>(bestShapelets->getNumberOfRows());

    if(uShapeletCount > 100) {
        std::size_t onePercentData = uShapeletCount*0.01;
        for(std::size_t i = 0; i < onePercentData; i++) {
            UShapelet* currentShapelet = setGap(i, bestShapelets, classNumber);
            randomIndices->setData(i, currentShapelet->getRandomIndex());

            delete currentShapelet;
        }
    }

    delete bestShapelets;
    delete randomIndices;
}

void UShapelets::findBestUShapelet() {
    findFastUShapelet();
}