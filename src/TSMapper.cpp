#include "TSMapper.h"

TSMapper::TSMapper(std::size_t uShapeLength, DataRow<float>* cutPoints) {
    this->uShapeLength = uShapeLength;
    this->cutPoints = cutPoints;

    symbolicData = nullptr;
    pointers = std::vector<std::size_t>();
}

TSMapper::~TSMapper() {
    delete cutPoints;
    
    if(symbolicData != nullptr)
        delete symbolicData;
}

std::size_t TSMapper::getNumberOfRows() {
    if(symbolicData == nullptr)
        return 0;
    else
        return symbolicData->getNumberOfRows();
}

DataContainer<int>* TSMapper::getData() {
    return symbolicData;
}

std::size_t TSMapper::getPointer(std::size_t index) {
    return pointers.at(index);
}

void TSMapper::printPointers() {
    for(std::size_t i = 0; i < pointers.size(); i++)
        std::cout << pointers.at(i) << " ";
    std::cout << std::endl;
}

void TSMapper::mapToString(DataRow<float>* paa, std::size_t alphabetSize, DataRow<int>* result) {
    for(std::size_t i = 0; i < paa->getNumberOfColumns(); i++) {
        std::size_t counter = 0;
        for(std::size_t j = 0; j < cutPoints->getNumberOfColumns(); j++) {
            if(cutPoints->getData(j) <= paa->getData(i))
                counter++;
            else
                break;
        }

        result->setData(i, counter);
    }
}

void TSMapper::timeseries2symbol(DataContainer<float>* uShapeletsZScore, std::size_t alphabetSize, std::size_t saxShapeletLength) {

    std::size_t windowSize = floor(((float) uShapeLength)/saxShapeletLength);

    if(pointers.size() > 0)
        pointers = std::vector<std::size_t>();
    
    if(symbolicData != nullptr)
        delete symbolicData;
    symbolicData = new DataContainer<int>(uShapeletsZScore->getNumberOfRows(), saxShapeletLength);

    std::size_t lastPointer = 0;

    for(std::size_t i = 0; i < uShapeletsZScore->getNumberOfRows(); i++) {
        DataRow<float>* dataRow = uShapeletsZScore->getRow(i);

        DataRow<float>* PAA;

        if(uShapeLength == saxShapeletLength)
            PAA = dataRow;
        else {
            if(uShapeLength % saxShapeletLength != 0) {
                PAA = new DataRow<float>(saxShapeletLength);

                std::size_t paaTotal = uShapeLength*saxShapeletLength;
                std::size_t valueIterator = 0;
                std::size_t paaIterator = 0;
                for(std::size_t j = 0; j < paaTotal; j++) {

                    PAA->addValue(paaIterator, dataRow->getData(valueIterator + 2));

                    if(j != 0 && (j + 1) % uShapeLength == 0)
                        paaIterator++;

                    if(j != 0 && (j + 1) % saxShapeletLength == 0)
                        valueIterator++;
                }

                PAA->scale(0, 0, 1.0/uShapeLength);
            } else {
                PAA = new DataRow<float>(saxShapeletLength);

                std::size_t paaTotal = uShapeLength*windowSize;
                std::size_t valueIterator = 0;
                std::size_t paaIterator = 0;
                for(std::size_t j = 0; j < paaTotal; j++) {

                    PAA->addValue(paaIterator, dataRow->getData(valueIterator + 2));

                    if(j != 0 && (j + 1) % uShapeLength == 0)
                        paaIterator++;

                    if(j != 0 && (j + 1) % windowSize == 0)
                        valueIterator++;
                }

                PAA->scale(0, 0, 1.0/uShapeLength);
            }
        }

        DataRow<int>* paaString = new DataRow<int>(saxShapeletLength);
        mapToString(PAA, alphabetSize, paaString);
        
        if(i == 0) {
            symbolicData->getRow(lastPointer)->setData(paaString->getRawPtr());
            pointers.push_back(lastPointer);
        } else if(!symbolicData->getRow(lastPointer)->compare(paaString)) {
            lastPointer++;
            symbolicData->getRow(lastPointer)->setData(paaString->getRawPtr());
            pointers.push_back(i);
        }

        delete PAA;
        delete paaString;
    }

    if(lastPointer + 1 != symbolicData->getNumberOfRows()) {
        DataContainer<int>* reshapedSymbolicData = new DataContainer<int>(lastPointer + 1, symbolicData->getNumberOfColumns());
        for(std::size_t i = 0; i < reshapedSymbolicData->getNumberOfRows(); i++)
            symbolicData->getRow(i)->copyValues(0, symbolicData->getNumberOfColumns(), reshapedSymbolicData->getRow(i)->getRawPtr(), 0);
        delete symbolicData;
        symbolicData = reshapedSymbolicData;
    }
}