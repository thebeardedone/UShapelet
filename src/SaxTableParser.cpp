#include "SaxTableParser.h"

SaxTableParser::SaxTableParser() {
}

DataRow<float>* SaxTableParser::getSaxTable(std::string filename, std::size_t alphabetSize) {

    DataRow<float>* dataRow = new DataRow<float>(alphabetSize);

    std::ifstream jsonFile(filename);

    boost::property_tree::ptree propertyTree;
    boost::property_tree::read_json(jsonFile, propertyTree);

    bool found = false;
    std::size_t counter = 0;
    for (auto & property: propertyTree) {
        if((std::size_t) stoi(property.first) == alphabetSize) {
            found = true;
            BOOST_FOREACH (boost::property_tree::ptree::value_type& itemPair, property.second) {
                if(itemPair.second.get_value<std::string>() == "-inf")
                    dataRow->setData(counter, -std::numeric_limits<float>::infinity());
                else
                    dataRow->setData(counter, boost::lexical_cast<float>(itemPair.second.get_value<std::string>()));
                counter++;
            }
            break;
        }
    }

    if(!found)
        throw new InvalidAlphabetSizeException;

    return dataRow;
}